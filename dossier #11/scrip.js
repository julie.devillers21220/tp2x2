  // Variable globale
  let index = 0;
  
  // Gestion des événements
  $('span').click(function () {
    // Récupération index
    console.log("jesaispas")
    let indexN = $('span').index(this);
  
    // Renouveller l'image
    $('#galerie>img').eq(index).fadeOut(1000).end().eq(indexN).fadeIn(1000);
  
    // Mettre à jour l'index
  index = indexN;
  });

// Création de la carte, vide à ce stade
let carte = L.map('carte', {
  center: [47.31, 5.069], // Centre de la France
  zoom: 5,
  minZoom: 4,
  maxZoom: 19,
});

// Ajout des tuiles (ici OpenStreetMap)
// https://wiki.openstreetmap.org/wiki/Tiles#Servers
L.tileLayer('https://a.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  attribution: '&copy; <a href="https://openstreetmap.org/copyright">OpenStreetMap contributors</a>',
}).addTo(carte);

// Ajout de l'échellae
L.control.scale().addTo(carte);



        // Obligatoire pour Codesandbox (pb de dépendances...)
        // Le fichier script.js est donc ici inutile...
        // Le script est placé à la fin du body car l'attribut defer ne fonctionne que pour des scripts externes
        // CE N'EST PAS UNE BONNE PRATIQUE !!!
  
        // Variable globale contenant l'état du lecteur
        let etatLecteur;
  
        function lecteurPret(event) {
          // event.target = lecteur
          event.target.setVolume(50);
        }
  
        function changementLecteur(event) {
          // event.data = état du lecteur
          etatLecteur = event.data;
        }
  
        let lecteur;
  
        function onYouTubeIframeAPIReady() {
          lecteur = new YT.Player("video", {
            height: "390",
            width: "640",
            videoId: "_H7CAgCahjk",
            playerVars: {
              color: "white",
              controls: 0,
              autoplay: 1,
              cc_load_policy: 0,
              enablejsapi: 1,
              modestbranding: 1,
              rel: 0
            },
            events: {
              onReady: lecteurPret,
              onStateChange: changementLecteur
            }
          });
        }
  
        // Hauteur de la vidéo
        const hauteurVideo = $("#video").height();
        // Position Y de la vidéo
        const posYVideo = $("#video").offset().top;
        // Valeur declenchant la modification de l'affichage (choix "esthétique")
        const seuil = posYVideo + 0.75 * hauteurVideo;
  
        // Gestion du défilement
        $(window).scroll(function () {
          // Récupération de la valeur du défilement vertical
          const scroll = $(window).scrollTop();
  
          // Classe permettant l'exécution du CSS
          $("#video").toggleClass(
            "scroll",
            etatLecteur === YT.PlayerState.PLAYING && scroll > seuil
          );
        });
